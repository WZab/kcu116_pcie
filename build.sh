#!/bin/bash
set -e
vivado -mode batch -notrace -nolog -nojournal -source eprj_create.tcl
vivado -mode batch -notrace -nolog -nojournal -source eprj_write.tcl
vivado -mode batch -notrace -nolog -nojournal -source eprj_build.tcl
