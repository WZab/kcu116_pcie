--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
--Date        : Fri Aug  2 17:16:48 2019
--Host        : WZsys1 running 64-bit Ubuntu 18.04.2 LTS
--Command     : generate_target bd1_wrapper.bd
--Design      : bd1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd1_wrapper is
  port (
    default_sysclk1_300_clk_n : in STD_LOGIC;
    default_sysclk1_300_clk_p : in STD_LOGIC;
    pci_express_x1_rxn : in STD_LOGIC;
    pci_express_x1_rxp : in STD_LOGIC;
    pci_express_x1_txn : out STD_LOGIC;
    pci_express_x1_txp : out STD_LOGIC;
    pcie_perstn : in STD_LOGIC;
    pcie_refclk_clk_n : in STD_LOGIC;
    pcie_refclk_clk_p : in STD_LOGIC
  );
end bd1_wrapper;

architecture STRUCTURE of bd1_wrapper is
  component bd1 is
  port (
    pcie_perstn : in STD_LOGIC;
    default_sysclk1_300_clk_p : in STD_LOGIC;
    default_sysclk1_300_clk_n : in STD_LOGIC;
    pcie_refclk_clk_p : in STD_LOGIC;
    pcie_refclk_clk_n : in STD_LOGIC;
    pci_express_x1_rxn : in STD_LOGIC;
    pci_express_x1_rxp : in STD_LOGIC;
    pci_express_x1_txn : out STD_LOGIC;
    pci_express_x1_txp : out STD_LOGIC
  );
  end component bd1;
begin
bd1_i: component bd1
     port map (
      default_sysclk1_300_clk_n => default_sysclk1_300_clk_n,
      default_sysclk1_300_clk_p => default_sysclk1_300_clk_p,
      pci_express_x1_rxn => pci_express_x1_rxn,
      pci_express_x1_rxp => pci_express_x1_rxp,
      pci_express_x1_txn => pci_express_x1_txn,
      pci_express_x1_txp => pci_express_x1_txp,
      pcie_perstn => pcie_perstn,
      pcie_refclk_clk_n => pcie_refclk_clk_n,
      pcie_refclk_clk_p => pcie_refclk_clk_p
    );
end STRUCTURE;
